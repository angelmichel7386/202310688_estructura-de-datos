<?php

use App\Http\Controllers\InterfazGraficaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\RecursividadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apuntadores', [InicioController:: class, 'inicio']);

Route::get('/memoria', [MemoriaController:: class, 'memoria']);

Route::get('/recursividad', [RecursividadController:: class, 'incrementable']);

Route::get('/interfaz', [InterfazGraficaController:: class, 'inicio']);

?>